export const highlightText = (text: string, highlightedText: string) => {
  const parts = text.split(new RegExp(`(${highlightedText})`, "gi"));

  return parts.map((part, index) => (
    <span key={index}>
      {part.toLowerCase() === highlightedText.toLowerCase() ? (
        <b style={{ backgroundColor: "yellow" }}>{part}</b>
      ) : (
        part
      )}
    </span>
  ));
};
