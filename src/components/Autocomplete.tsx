import { useState, useEffect, useRef } from "react";
import { DogBreed } from "../types";
import { highlightText } from "../utils/highlightText";

const Autocomplete = () => {
  const [value, setValue] = useState<string>("");
  const [displaySuggestions, setDisplaySuggestions] = useState<boolean>(false);
  const [options, setOptions] = useState<DogBreed[]>([]);

  const ref = useRef<HTMLDivElement>(null);

  const searchDogName = async (dogName: string) => {
    try {
      const response = await fetch(
        `https://api.api-ninjas.com/v1/dogs?name=${dogName}`,
        {
          method: "GET",
          headers: { "X-Api-Key": "bBCGSdEPNpsqJMHvhHpQmg==1cC4s4R4o42XTRHU" },
        }
      );
      const dogs = await response.json();
      setOptions(dogs);
    } catch (e) {
      console.error(e);
    }
  };
  useEffect(() => {
    const delayDebounceFn = setTimeout(() => {
      value.length > 2 && searchDogName(value);
    }, 1000);

    return () => clearTimeout(delayDebounceFn);
  }, [value]);

  useEffect(() => {
    const handleClick = (event: Event) => {
      ref.current &&
        !ref.current.contains(event.target as Element) &&
        setDisplaySuggestions(false);
    };
    document.addEventListener("click", handleClick);
    return () => {
      document.removeEventListener("click", handleClick);
    };
  }, []);

  const handleChange = async (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue(event.target.value);
    event.target.value.length === 0 && setOptions([]);
  };

  const handleClick = (suggestion: DogBreed) => {
    setValue(suggestion.name);
    setDisplaySuggestions(false);
  };

  return (
    <div className="autocomplete" ref={ref}>
      <input
        placeholder="Search for a dog breed"
        value={value}
        onChange={handleChange}
        onFocus={() => setDisplaySuggestions(true)}
      />
      {displaySuggestions && !!options.length && (
        <ul className="suggestions">
          {options.map((suggestion: DogBreed) => (
            <li onClick={() => handleClick(suggestion)} key={suggestion.name}>
              {highlightText(suggestion.name, value)}
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default Autocomplete;
