import './App.css';
import Autocomplete from './components/Autocomplete';

const App = () => {
  return (
    <div>
     <Autocomplete />
    </div>
  );
}

export default App;
