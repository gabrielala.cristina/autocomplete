# 1. What is the difference between Component and PureComponent? Give an example where it might break my app.

Component does not implement the shouldComponentUpdate() -  which means that it will re-render every time its parent component re-renders, even if the component's props and state haven't changed.
PureComponent implements the shouldComponentUpdate() and makes a shallow comparison of the props and state, determining whether a re-render needs to be done.

PureComponent might break the app when there are complex data structures in the state or the props and the shallow comparison doesn't properly compare them.

# 2. Context + ShouldComponentUpdate might be dangerous. Why is that?

Using shouldComponentUpdate with Context can lead to maintenance issues, because the shouldComponentUpdate checks whether a component should re-render based on its props and state, not based on the context,
therefore re-renders might not be triggered as expected.

# 3. Describe 3 ways to pass information from a component to its PARENT.

1. Context API: creates a shared context between components that can be accessed and modified by child components directly.
2. State management libraries (e.g Redux): providing a global store that can be accessed and updated by any component, allowing the data to be passed between child and parent components.
3. Callback functions: passing a callback function as a prop from the parent component to the child component, so that when an action occurs in the child component, it can call the callback function, with the necessary data.

# 4. Give 2 ways to prevent components from re-rendering.

1. Wrapping a functional component with React.memo HOC, memoizing the component, only re-rendering when its props have changed. 
2. Using shouldComponentUpdate lifecycle method for class components, determining whether the component should re-render based on changes to its props and state.

# 5. What is a fragment and why do we need it? Give an example where it might break my app.

A fragment is a way to group multiple children elements without adding extra elements to the DOM. <> children </>
We use it when we need a single parent element for rendering a component's children, improving the structure and readability of the code.

# 6. Give 3 examples of the HOC pattern.

1. Authentication HOC - used to protect specific components/routes
2. Data fetching HOC - used to fetch data from the API and pass that data to the wrapped component
3. Form handling HOC - used for adding form handling functionality (like input change, submission, validations)

# 7. What's the difference in handling exceptions in promises, callbacks and async...await?

Promises offer centralized error handling with the .catch() method.
Callbacks offer a more decentralized error handling, making it easier to miss exceptions.
Async...await, offers the try...catch to catch exceptions synchronously, which is more natural and can make error handling more intuitive. (I prefer this one personally)

# 8. How many arguments does setState take and why is it async?

'setState' method takes up to 2 arguments, but we usually pass only one. Being async, it does not instantly change the state, it creates a pending state, so accessing it right after calling the setState() returns the old value, not the updated one.

# 9. List the steps needed to migrate a Class to Function Component.

1. Understand what functionality of the Class Component is
2. Create a new file for the Functional Component
3. Take the props and state from the Class Component and copy them to the new Functional Component
4. Take the JSX code in the return statement and add it to the Functional Component
5. Make use of the 'useEffect' hook to replace any old lifecycle methods
6. Move the methods from the Class Component to the Functional Component
7. Replace the Class Component's state with the 'useState' hook
8. Make sure the props remain the same
9. Add new unit tests for the Functional Component
10. Update imports, dependencies
11. Remove the unused Class Component from the codebase

# 10. List a few ways styles can be used with components.

1. Inline styling - applying style directly on a component by passing a style object to the style attribute.
2. CSS - importing a CSS file into the component file and using the styles from that CSS file
3. Libraries (Styled Components) - allowing CSS styles to be written directly within the code.

# 11. How to render an HTML string coming from the server.

1. Create a component that will render the HTML string by passing an object with a __html key to the dangerouslySetInnerHTML attribute
 <div dangerouslySetInnerHTML={{ __html: htmlString }} />

2. Use the previously created component and send the HTML string as a prop

